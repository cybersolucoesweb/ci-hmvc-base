README
======

### DESCRIÇÃO DO PROJETO ###

    Uma breve descrição do projeto

### INÍCIO E PRAZO ###

    Iniciado em: 
    Prazo de Entrega: 

__PACOTES__

    [CodeIgniter 3.0.5] [http://www.codeigniter.com/]
    [HMVC para CodeIgniter 3x] [https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc]
    
__REPOSITÓRIO GIT__

    [BITBUCKET] https://bitbucket.org/cybersolucoesweb/ci-hmvc-base
    
__GIT REMOTE E GIT PUSH__

    [BITBUCKET] 
    - git git remote add origin git@bitbucket.org:cybersolucoesweb/ci-hmvc-base.git
    - git push origin master

__DOCUMENTAÇÃO__

    Nenhuma documentação adicionada
    

NOME DO PROJETO - Copyright 2015 - Todos os direitos reservados - Desenvolvedor [NOME DO DESENVOLVEDOR][link]
[link]: http://www.site.com.br