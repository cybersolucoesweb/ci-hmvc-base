<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Erros Personalizados
 * Altere conforme desejar
 *
 * @package		Erros
 * @version		1.0
 * @author 		Junior Marquezano <juniormarquezano@gmail.com>
 * @copyright 	Copyright (c) 2016
 */
class Erros extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        // Gravamos um log do erro
        log_message('error', '404 Page Not Found --> ' . $_SERVER['REQUEST_URI']);

        $data['page'] = base_url($_SERVER['REQUEST_URI']);

        $this->load->view('error_404', $data);
    }
}

