<?php die('This file is not really here!');

/**
 * ------------- NÃO É NECESSÁRIO O UPLOAD DO ARQUIVO PARA O SERVIDOR ---------------------
 *
 * Implementa o auto complete do Codeigniter ao PHPStorm
 * Todos os indices, classes e construct serão carregados no projeto.
 *
 * Algumas propriedades desse arquivo foram retirados da internet
 * Está é apenas uma maneira de implementar melhor o auto complete sem ter que editar arquivos principais do CI
 *
 * PHP version 5 >
 * Codeigniter 3 ou superior
 *
 * LICENSE: GPL http://www.gnu.org/copyleft/gpl.html
 *
 * @category
 * @package    CodeIgniter auto_complete.php
 * @author     Junior Marquezano
 * @copyright  2016
 * @version    1.0
 */

/**
 * @property CI_DB $db
 * @property CI_DB_forge $dbforge
 * @property CI_User_Agent $agent
 * @property CI_Benchmark $benchmark
 * @property CI_Cache $cache
 * @property CI_Calendar $calendar
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Driver_Library $driver
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Encryption $encryption
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Jquery $jquery
 * @property CI_Lang $lang
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Migration $migration
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Security $security
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Utf8 $utf8
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 *
 * Para ter o auto complete das suas library e models, coloque-os abaixo no mesmo padrão acima
 *
 * LIBRARIES
 *
 * MODELS
 *
 */
class CI_Controller {};
class MY_Controller extends CI_Controller {};

/**
 * @property CI_DB $db
 * @property CI_DB_forge $dbforge
 * @property CI_User_Agent $agent
 * @property CI_Benchmark $benchmark
 * @property CI_Cache $cache
 * @property CI_Calendar $calendar
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Driver_Library $driver
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Encryption $encryption
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Jquery $jquery
 * @property CI_Lang $lang
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Migration $migration
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Security $security
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Utf8 $utf8
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 *
 */
class CI_Model {};

/* End of file autocomplete.php */
/* Location: ./application/config/autocomplete.php */